app.controller('Controller', function($scope, $http, $sce) {

  $scope.generate = function() {
    window.location = Math.floor(Math.random() * 10000);
  };

  $scope.send = function(index) {
    var data = {};
    data.id = window.location.pathname.split('/')[2];
    $scope.list.map( function(i) {
      if(i.name !== 'id') {
        data[i.name] = i.value;
      }
    });
    $http.post('/request/' + data.id, JSON.stringify([data])).success(function(data) {
      console.log(data)
      $scope.response = data;
    }).error(function(err) {
      console.log(err);
    });

  };

  $scope.list = [
    {
      name: "area",
      value: "area.ru.r4.a44.628"
    },{
      name: "rating",
      value: 2.000
    },{
      name: "inn",
      value: "1234567890"
    },{
      name: "kpp",
      value: "987654321"
    },{
      name: "name",
      value: "Название компании"
    },{
      name: "name_full",
      value: "Общество с ограниченной ответственностью \"Название компании\""
    },{
      name: "address",
      value: "420087, Россия, Казань, Ленина, 15"
    },{
      name: "address_legal",
      value: "420073 Казань Ленина 15, Россия"
    },{
      name: "anno_short",
      value: "продажа спецодежды, спецобуви, средств индивидуальной защиты"
    },{
      name: "anno",
      value: "Более полное описание"
    },{
      name: "phone",
      value: "(843) 295-12-34"
    },{
      name: "fax",
      value: "(843) 295-12-35"
    },{
      name: "site",
      value: ""
    },{
      name: "location",
      value: ["Россия","Приволжский","Республика Татарстан","Казань"]
    }
  ];
});
