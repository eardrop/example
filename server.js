var path = require('path'),
    bodyParser = require('body-parser'),
    express = require('express'),
    app = express(),
    response = require('./response.json');

app.set('port', process.env.PORT );
app.use(express.static(path.join(__dirname, '/static')));
app.use(bodyParser.json());

app.get('/', function(req, res, next) {
  res.redirect('/companies/23801');
});
app.get('/companies/:id', function(req, res, next) {
  res.sendFile(__dirname + '/static/views/layout.html');
});

app.post('/request/:id', function(req, res, next) {
  response.unshift(req.body[0]);
  res.send(response);
});

app.listen(9000);
